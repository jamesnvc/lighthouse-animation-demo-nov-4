//
//  ViewController.swift
//  AnimationStuff
//
//  Created by James Cash on 04-11-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = 50.0

        let subLayer = CALayer()
        subLayer.frame = CGRect(origin: self.view.center, size: CGSize(width: 200, height: 200))
        subLayer.backgroundColor = UIColor.redColor().CGColor
        self.view.layer.addSublayer(subLayer)
//
//        let fadeAnim = CABasicAnimation(keyPath: "opacity")
//        fadeAnim.fromValue = 1.0
//        fadeAnim.toValue = 0.0
//        fadeAnim.duration = 3.0
//        fadeAnim.autoreverses = true
//        fadeAnim.repeatCount = HUGE
//        subLayer.addAnimation(fadeAnim, forKey: "opacityAnimation")

//        let moveAnimation = CABasicAnimation(keyPath: "position.y")
//        moveAnimation.fromValue = self.view.center.y
//        moveAnimation.toValue = 0.0
//        moveAnimation.duration = 2.0
//        moveAnimation.repeatCount = HUGE
//        subLayer.addAnimation(moveAnimation, forKey: "movingY")

        let cornerChange = CABasicAnimation(keyPath: "cornerRadius")
        cornerChange.fromValue = 0
        cornerChange.toValue = 50
        cornerChange.autoreverses = true
        cornerChange.duration = 1.5
        cornerChange.repeatCount = HUGE
        subLayer.addAnimation(cornerChange, forKey: "cornerGrowing")

        let colorAnimation = CAKeyframeAnimation(keyPath: "backgroundColor")
        colorAnimation.values = [
            UIColor.redColor().CGColor,
            UIColor.blueColor().CGColor,
            UIColor.greenColor().CGColor,
            UIColor.grayColor().CGColor,
            UIColor.brownColor().CGColor,
        ]
        colorAnimation.autoreverses = true
        colorAnimation.duration = 3.0
        colorAnimation.repeatCount = HUGE
        subLayer.addAnimation(colorAnimation, forKey: "colourChanging")

        let yMovingAnimation = CAKeyframeAnimation(keyPath: "position.y")
        let top = CGRectGetMinY(self.view.frame)
        let bottom = CGRectGetMaxY(self.view.frame)
        yMovingAnimation.values = [
            self.view.center.y,
            top + 200,
            bottom - 200,
            top,
            bottom,
        ]
        yMovingAnimation.keyTimes = [
            0, // center
            0.1, // top + 200
            0.5, // bottom - 200
            0.6, // top
            1, // bottom
        ]
        yMovingAnimation.duration = 3.0
        yMovingAnimation.repeatCount = HUGE
//        subLayer.addAnimation(yMovingAnimation, forKey: "yMoving")

        let pathAnimation = CAKeyframeAnimation(keyPath: "position")
        let path = CGPathCreateMutable()
        CGPathMoveToPoint(path, nil, 0, 0)
        CGPathAddArc(path, nil, 100, 100, 50, 0, 90, true)
        CGPathAddLineToPoint(path, nil, 200, 450)
        CGPathAddQuadCurveToPoint(path, nil, 87, 42, 76, 88)
        pathAnimation.path = path
        pathAnimation.autoreverses = true
        pathAnimation.rotationMode  = kCAAnimationRotateAuto
        pathAnimation.repeatCount = 3
        pathAnimation.duration = 4.0
        pathAnimation.delegate = self
        subLayer.addAnimation(pathAnimation, forKey: "movingLikeCrazy")

    }

    override func animationDidStart(anim: CAAnimation) {
        print("started path")
    }

    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        print("Finished!")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

